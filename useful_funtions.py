import numpy as np
from typing import List, Union
from scipy.integrate import solve_ivp
from units import GM, r_0, use_parallel
from numba import jit, prange


def conditional_decorator(dec, condition):
    def decorator(func):
        if not condition:
            # Return the function unchanged, not decorated.
            return func
        return dec(func)
    return decorator


def gaussian(z: np.ndarray, h_p: float, Sigma0: float):
    """Returns a gaussian (isothermal vertical hydrostatic equilibrium with linearized gravity)."""
    return Sigma0 / np.sqrt(2 * np.pi) / h_p * np.exp(-z ** 2 / 2 / h_p ** 2)


def curved_gaussian(z: np.ndarray, h_p: float, Sigma0: float):
    """Returns an isothermal vertical hydrostatic equilibrium with proper gravity."""
    return Sigma0 / np.sqrt(2 * np.pi) / h_p * np.exp((r_0/h_p)**2 * ((1 + (z/r_0)**2)**-.5 - 1))


def linearized_equations(
    t: float, x: List[Union[float, int]], psi: float, alpha: float, q: float
) -> List[Union[int, float]]:
    """Eqs. (61)-(64) from Dullemond et al. (2021).

    :param t: Time.
    :param x: List of variables H, V_x, V_y, V_z.
    :param psi: Warp strength.
    :param alpha: Turbulent viscosity parameter.
    :return: Functions evaluated at t, x.
    """
    H, vx, vy, vz = x

    cos = np.cos(t)
    sin = np.sin(t)
    cos2 = cos * cos
    psi2 = psi * psi

    del_tau_lnH = psi * cos * vx + vz

    Fx = -alpha * ((4 / 3 * psi2 * cos2 + 1) * vx + 1 / 3 * psi * cos * vz + psi * sin)
    Fy = -alpha * ((psi2 * cos2 + 1) * vy - q * psi * cos)
    Fz = -alpha * ((psi2 * cos2 + 4 / 3) * vz + 1 / 3 * psi * cos * vx + psi2 * sin * cos)
    return [
        H * del_tau_lnH,
        -vx * del_tau_lnH + 2 * vy + psi * cos / H ** 2 + Fx,
        -vy * del_tau_lnH - (2 - q) * vx + Fy,
        -vz * del_tau_lnH - psi * sin * vx + H ** -2 - 1 + Fz,
    ]


def solve_linearized_equations(t: float, x0: List[Union[int, float]], psi: float, alpha: float, q=1.5):
    """Solve eqs. (61)-(64) from Dullemond et al. (2021) using scipy.integrate.solve_ivp.

    Starting time is always 0, end time is given by parameter t.
    For the equations that are solved, see function linearized_equations.

    Example
    --------
    >>> sol = solve_linearized_equations(t=1, x0=[1, 0, 0, 0], psi=0.1, alpha=1e-3, q=q)
    >>> time = np.linspace(0, 1, 100)
    >>> H, V_x, V_y, V_z = sol.sol(time)
    >>> print(V_x[:5])
    [0.         0.00101007 0.00201994 0.0030294  0.00403824]
    """
    return solve_ivp(linearized_equations, [0, t], x0, dense_output=True, args=(psi, alpha, q))


def estimate_coef(x, y, intercept=True, return_b0=True):
    """Linear least-squares regression for y = b_0 + b_1*x."""
    if intercept:
        # number of observations/points
        n = np.size(x)

        # mean of x and y vector
        m_x = np.mean(x)
        m_y = np.mean(y)

        # calculating cross-deviation and deviation about x
        SS_xy = np.sum(y * x) - n * m_y * m_x
        SS_xx = np.sum(x * x) - n * m_x * m_x

        # calculating regression coefficients
        b_1 = SS_xy / SS_xx
        b_0 = m_y - b_1 * m_x

    else:
        # assume a line though the origin
        b_1 = np.sum(x * y) / np.sum(x ** 2)
        b_0 = 0

    if return_b0:
        return b_0, b_1

    return b_1


def get_V(sim: "Sim", width: float, time_index: int, intercept: bool = False, return_b0: bool = False):
    """Estimate the slope of the velocities in the domain [-width, width] at time step time_index."""
    i_left = np.argmin(np.abs(sim.z_s_g + width))
    i_right = np.argmin(np.abs(sim.z_s_g - width))

    z = sim.z_s_g[i_left:i_right]
    data = sim.data[time_index]

    coefs = tuple(estimate_coef(z, getattr(data, dim)[i_left:i_right], intercept=intercept, return_b0=return_b0) for dim in ("vx", "vy", "vz"))

    return coefs


def vip0(a, psi, q=3/2):
    """Eqs. 88 and 89 of Dullemond et al. 2021."""
    k2 = 2*(2-q)
    vxp0 = (a*(4-k2) + 1j*(1+a**2)) / (k2 + (1j+a)**2) * psi
    vyp0 = (2*a*(1j+a) - .5*k2*(a**2+2j*a+1)) / (k2 + (1j+a)**2) * psi
    return vxp0.real, vyp0.imag


def c2s(x):
    """Centered to staggered."""
    return (x[1:] + x[:-1]) / 2


def TSC(target_pos, source_pos, source_val, dz):
    """Triangualar shaped clouds interpolation"""
    # TODO: where should be a less and where a less equal?
    dist = np.abs(target_pos[:, None] - source_pos)
    # dist_close = (dist <= dz / 2)
    # dist_far = np.logical_and(dz / 2 < dist, dist < 3 * dz / 2)
    weight_close = (3 / 4 - (dist / dz) ** 2) * (dist <= dz / 2)
    weight_far = (.5 * (3 / 2 - dist / dz) ** 2) * np.logical_and(dz / 2 < dist, dist < 3 * dz / 2)
    return np.sum(weight_close*source_val + weight_far*source_val, axis=1)


@jit(fastmath=True, parallel=use_parallel, cache=True)
def TSC_jit(target_pos, source_pos, source_val, dz):
    N_target = len(target_pos)
    N_source = len(source_pos)
    target_val = np.zeros_like(target_pos)

    dz_half = dz / 2
    dz_three_halves = 3 * dz / 2

    for i in prange(N_target):
        for j in prange(N_source):
            dist = abs(target_pos[i] - source_pos[j])

            if dist > dz_three_halves:
                continue

            if dist <= dz_half:
                weight = .75 - (dist / dz) ** 2
            else:
                weight = .5 * (3 / 2 - dist / dz) ** 2

            target_val[i] += source_val[j] * weight

    return target_val


@jit(fastmath=True, parallel=use_parallel, cache=True)
def TSC_fast(target_pos, source_pos, source_val1, source_val2, dz):
    """!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    IMPORTANT
    This only works if source_pos is already in sorted order
    e.g. the coordinate system
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    """
    N_target = len(target_pos)
    N_source = len(source_pos)
    target_val1 = np.zeros_like(target_pos)
    target_val2 = np.zeros_like(target_pos)

    dz_half = dz / 2
    dz_three_halves = 3 * dz / 2

    tp_argsort = np.argsort(target_pos)
    tp_sort = target_pos[tp_argsort]

    start_ind = 0

    for i in prange(N_target):
        ind = tp_argsort[i]
        for j in prange(start_ind, N_source):
            dist = tp_sort[i] - source_pos[j]

            if dist > dz_three_halves:
                start_ind += 1
                continue

            if dist < -dz_three_halves:
                break

            dist = abs(dist)

            if dist <= dz_half:
                weight = .75 - (dist / dz) ** 2
            else:
                weight = .5 * (1.5 - dist / dz) ** 2

            target_val1[ind] += source_val1[j] * weight
            target_val2[ind] += source_val2[j] * weight

    return target_val1, target_val2


def variable_psi(t, t_shift, width):
    """Derivative of a Gaussian, max height is 1
    t: time
    t_shift: Shift along t (psi at t_shift is zero)
    width: Width of the Gaussian in units of local scale height, equals distance between max and min"""
    return -np.exp(-(t-t_shift)**2/width**2/2) * (t-t_shift) / width * np.sqrt(np.exp(1))