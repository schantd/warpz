import numpy as np
import matplotlib.pyplot as plt
from typing import List, Optional, Union, Callable
from timeit import default_timer
from useful_funtions import gaussian, curved_gaussian, get_V, vip0, conditional_decorator, c2s, TSC, TSC_jit, TSC_fast, variable_psi
from temperature_functions import T_irr, T_irr_shadowing
from units import GM, r_0, use_numba, use_cache, use_parallel
from numba import jit
from copy import deepcopy
from pathlib import Path
import pickle


class State(object):
    __slots__ = ["rho", "vx", "vy", "vz", "t"]

    def __init__(self, rho, vx, vy, vz, t):
        self.rho = rho
        self.vx = vx
        self.vy = vy
        self.vz = vz
        self.t = t


class BoundaryConditions:
    def __init__(self, bc_rho: str, bc_vx: str, bc_vy: str, bc_vz: str,
                 damp_rho=False, damp_vx=False, damp_vy=False, damp_vz=False):
        self.bc_rho = bc_rho
        self.bc_vx = bc_vx
        self.bc_vy = bc_vy
        self.bc_vz = bc_vz

        self.damp_rho = damp_rho
        self.damp_vx = damp_vx
        self.damp_vy = damp_vy
        self.damp_vz = damp_vz


@conditional_decorator(jit(cache=True), use_numba)
def fill_ghost(x, boundary_type):
    if boundary_type == "open":
        x[0] = x[1]
        x[-1] = x[-2]
        return x
    elif boundary_type == "zero":
        x[0] = 0
        x[-1] = 0
    elif boundary_type == "exponential":
        log_x = np.log(x)  # [[0, 1, 2, -3, -2, -1]])
        dummy_left = 2 * log_x[1] - log_x[2]
        dummy_right = 2 * log_x[-2] - log_x[-3]
        ghost_value_left, ghost_value_right = np.exp(dummy_left), np.exp(dummy_right)
        # TODO: ghost value should be smaller(?)
        # assert ghost_value_left < x[1]
        # assert ghost_value_right < x[-2]
        x[0] = ghost_value_left
        x[-1] = ghost_value_right
    elif boundary_type == "oni":
        x[0] = -abs(x[1])
        x[-1] = abs(x[-2])
    elif boundary_type == "reflective":
        x[0] = -x[1]
        x[-1] = -x[-2]
    elif boundary_type == "periodic":
        x[0] = x[-2]
        x[-1] = x[1]
    else:
        raise ValueError

    return x


class DustClass:
    def __init__(self, X, St, diffusion=True):
        self.X = X  # (4, N)
        self.St = St
        self.diffusion = diffusion
        self.coupling = None


class Sim:
    def __init__(self, initial_conditions: State, boundaries: BoundaryConditions,
                 psi: float, z_ghost: np.ndarray, cs: Union[float, np.ndarray, Callable], alpha_t: float,
                 logs=False, curvature=False, dust: DustClass = None, psi_var=None, **kwargs):
        """Possible kwargs:
        c_max: Default 0.5. Fudge factor for CFL-condition and viscosity time step.
        q: Default 3/2.
        damping_tau: default 33. Damping time scale (in units of inverse local orbital frequency).
        damping_z_fraction: Default 0.8: Domain in which to damp, z_damp goes from damping_z_fraction*z_max to z_max.
        c_bulk: default 3: approx. #cells to smear out shocks in vNR artificial viscosity.
        save_single: default True: if True, save each output individually. Otherwise, save everything in the Sim class.
        """
        self.initial_conditions = initial_conditions
        self.boundaries = boundaries

        self.rho = np.copy(initial_conditions.rho)
        self.vx = np.copy(initial_conditions.vx)
        self.vy = np.copy(initial_conditions.vy)
        self.vz = np.copy(initial_conditions.vz)
        self.t = initial_conditions.t
        assert len(self.rho) - 1 == len(self.vx) == len(self.vy) == len(self.vz)

        # 2 ghost cell variables
        self.rho_g = np.zeros(len(self.rho) + 2)
        self.mom_xm = np.zeros_like(self.rho)
        self.mom_xp = np.zeros_like(self.rho)
        self.mom_ym = np.zeros_like(self.rho)
        self.mom_yp = np.zeros_like(self.rho)
        self.mom_zm = np.zeros_like(self.rho)
        self.mom_zp = np.zeros_like(self.rho)

        self.logs = logs
        self.C_max = kwargs.get("c_max", 0.4)  # CFL-number
        self.C_bulk = kwargs.get("c_bulk", 3.)  # artificial viscosity constant (vNR)
        self.N_steps_total = 0

        self.q = kwargs.get("q", 3/2)
        self.psi = psi  # this variable changes if psi_var is given
        self.psi_var = psi_var  # tuple with (t_shift, width)
        self.psi0 = psi  # this variable never changes

        # all coordinates and cell sizes for centered and staggered grid, with and without ghost cells
        self.z_c = z_ghost[1:-1]                        # N-2
        self.z_c_g = z_ghost                            # N
        self.z_s = (self.z_c[1:] + self.z_c[:-1]) / 2   # N-3
        self.z_s_g = (z_ghost[1:] + z_ghost[:-1]) / 2   # N-1
        self.dz = self.z_c[1] - self.z_c[0]             # 1
        self.dz_c = self.z_c[1:] - self.z_c[:-1]        # N-3
        self.dz_c_g = self.z_c_g[1:] - self.z_c_g[:-1]  # N-1
        self.dz_s = self.z_s[1:] - self.z_s[:-1]        # N-4
        self.dz_s_g = self.z_s_g[1:] - self.z_s_g[:-1]  # N-2

        self.Omega_0 = np.sqrt(GM/r_0**3)

        # gravity
        if curvature:
            self.grav_force = - GM / (r_0**2 + self.z_s**2)**(3/2) * self.z_s
        else:
            self.grav_force = - self.Omega_0**2 * self.z_s

        # for a constant sound speed
        if isinstance(cs, float) or isinstance(cs, np.ndarray):
            self.cs_is_time_dependent = False
            self.cs_func = None

            self.cs = np.ones_like(self.rho) * cs
            self.cs2 = self.cs ** 2
            self.cs_s = c2s(self.cs)
            self.cs2_s = c2s(self.cs2)
        # for a time-dependent sound speed
        elif callable(cs):
            if self.logs:
                print("Sound speed is time-dependent.")
            self.cs_is_time_dependent = True
            self.cs_func = cs

            self.cs = self.cs_func(phi=self.Omega_0 * self.t)
            self.cs2 = self.cs ** 2
            self.cs_s = c2s(self.cs)
            self.cs2_s = c2s(self.cs2)
        else:
            raise ValueError

        self.alpha_t = alpha_t
        ###################################################################
        # TODO: WHAT IS NU FOR A NON-ISOTHERMAL DISK????? #################
        # currently it assumes the central sound speed for the whole disk #
        ###################################################################
        self.nu = alpha_t * self.cs2[len(self.cs2)//2] / self.Omega_0

        ##########################
        # damping initialization #
        ##########################
        if any([getattr(boundaries, x) == True] for x in dir(boundaries)):
            tau_damp = kwargs.get("damping_tau", 33) / self.Omega_0  # smaller tau -> stronger damping
            damping_zone = kwargs.get("damping_z_fraction", .8)
            z_max = self.z_c[-1]
            z_bound = damping_zone * z_max

            ramp_r = (z_max - z_bound) ** -2 * (self.z_c_g - z_bound) ** 2 * (self.z_c_g > z_bound)
            ramp_l = (z_bound - z_max) ** -2 * (self.z_c_g + z_bound) ** 2 * (self.z_c_g < -z_bound)
            ramp_c = ramp_l + ramp_r

            ramp_r = (z_max - z_bound) ** -2 * (self.z_s_g - z_bound) ** 2 * (self.z_s_g > z_bound)
            ramp_l = (z_bound - z_max) ** -2 * (self.z_s_g + z_bound) ** 2 * (self.z_s_g < -z_bound)
            ramp_s = ramp_l + ramp_r

            self.inv_damp_c = ramp_c / tau_damp
            self.inv_damp_s = ramp_s / tau_damp

        self.data = []
        self.use_numba = True
        self.last_dt = []

        if dust is None:
            self.use_dust = False
        else:
            self.use_dust = True
            self.dust = dust
            self.dust.coupling = self.Omega_0 / self.dust.St
            self.data_d = []

        self.save_single = None
        self.output_dir = None
        self.N_dt = None

    def simulate(self, dt_output: float, N_dt: int, output_dir=None, save_single=True):
        self.save_single = save_single
        self.N_dt = N_dt

        if self.save_single:
            assert output_dir is not None
            try:
                self.output_dir = "outputs/" + output_dir
                Path(self.output_dir).mkdir(parents=False)
                self.N_out = 0  # output number
            except FileNotFoundError as fnfe:
                print("No output directory yet. Please create directory called 'outputs'.")
                raise fnfe
            except FileExistsError as fee:
                print(f"The directory {self.output_dir} already exists. Please choose a different name.")
                raise fee
        # save parameters and boundary conditions
        if self.save_single:
            with open(f"{self.output_dir}/params.pkl", "wb") as outfile:
                _dummy = deepcopy(self.__dict__)
                del _dummy["initial_conditions"]
                del _dummy["boundaries"]
                pickle.dump(_dummy, outfile, pickle.HIGHEST_PROTOCOL)
                del _dummy
            with open(f"{self.output_dir}/bounds.pkl", "wb") as outfile:
                pickle.dump(self.boundaries.__dict__, outfile, pickle.HIGHEST_PROTOCOL)
        self.__save_data()

        t_end = self.t + dt_output * N_dt
        if self.logs:
            print(f"Simulations starts at t={self.t} and ends at t={t_end}.")
        assert self.t < t_end, "Starting time t must be smaller than end time t_end"
        steps_between_outputs = 0

        # the appropriate time step for viscosity has to be calculated only once
        # the factor 0.3 is a huge mystery and was determined empirically
        if self.nu > 0:
            dt_visc_m2 = (.25 * np.min(self.dz_s_g ** 2 / self.nu)) ** -2  # before: .15
        else:
            dt_visc_m2 = 0.

        # sound speed time step
        dt_sound_m2 = (self.dz / np.max(self.cs)) ** -2

        start_time = default_timer()
        while self.t < t_end:
            # TODO: make cfl for advection velocity prettier
            # TODO: phi at half time step
            phi = self.Omega_0 * self.t
            if self.psi_var is not None:
                self.psi = self.psi0 * variable_psi(self.t, *self.psi_var)

            if self.cs_is_time_dependent:
                self.cs = self.cs_func(phi=self.Omega_0 * self.t)
                self.cs2 = self.cs ** 2
                self.cs_s = c2s(self.cs)
                self.cs2_s = c2s(self.cs2)

            advec_vel = self.vz + self.psi * np.cos(phi) * self.vx

            # time step calculation
            # _m2 stands for **-2
            dt_fluid_m2 = (np.max(advec_vel) / self.dz) ** 2
            dt_artif_m2 = (4 * self.C_bulk * np.max(np.abs((advec_vel[1:] - advec_vel[:-1]) / self.dz))) ** 2
            dt = self.C_max * (dt_fluid_m2 + dt_artif_m2 + dt_visc_m2 + dt_sound_m2) ** -.5

            time_until_output = dt_output - self.t % dt_output

            # second part catches a round-off error in the time_until_output calculation, e.g 1%0.2 != 0
            # otherwise we will get stuck in an infinite loop
            # TODO: check if the last time step was saved before doing a time step when a rounding error occurs
            if dt < time_until_output or self.t + time_until_output == self.t:
                self.step(dt=dt, phi=phi)
                steps_between_outputs += 1
            else:
                self.step(dt=time_until_output, phi=phi)
                self.__save_data()
                self.last_dt.append(time_until_output)

                if self.logs:
                    print(f"we are at step {self.N_steps_total}  last dt={dt:.5f}  "
                          f"t={self.t:.2f}  steps between outputs={steps_between_outputs}")
                steps_between_outputs = 0

        end_time = default_timer()
        print(f"Simulation finished after {end_time - start_time:.2f} seconds.")

    def step(self, dt, phi):
        # Fill ghost cells
        self.vx = fill_ghost(self.vx, boundary_type=self.boundaries.bc_vx)
        self.vy = fill_ghost(self.vy, boundary_type=self.boundaries.bc_vy)
        self.vz = fill_ghost(self.vz, boundary_type=self.boundaries.bc_vz)
        self.rho = fill_ghost(self.rho, boundary_type=self.boundaries.bc_rho)

        ##########################################
        # substep 1: Sources #####################
        ##########################################
        # the ordinary part of vx and vy are updated with the midpoint method
        vx_ord = dt * (2 * self.vy + dt * (self.q - 2) * self.vx) * self.Omega_0
        vy_ord = dt * ((self.q - 2) * (self.vx + dt * self.vy)) * self.Omega_0
        vz_ord = - dt * self.psi * np.sin(phi) * (self.vx + dt * self.vy) * self.Omega_0

        # pressure force calculation
        log_rho = np.log(self.rho[1:-1])
        pressure_force = - self.cs2_s[1:-1] * (log_rho[1:] - log_rho[:-1]) / self.dz_c

        # velocity update
        self.vx[1:-1] += (self.psi * np.cos(phi) * pressure_force) * dt + vx_ord[1:-1]
        self.vy[1:-1] += vy_ord[1:-1]
        self.vz[1:-1] += (pressure_force + self.grav_force) * dt + vz_ord[1:-1]

        # von Neumann-Richtmyer artificial viscosity
        advec_vel = self.vz + self.psi * np.cos(phi) * self.vx
        d_vz = advec_vel[1:] - advec_vel[:-1]
        art_visc = -self.C_bulk * self.rho[1:-1] * d_vz ** 2 * (d_vz < 0)
        rho_s = (self.rho[1:] + self.rho[:-1]) / 2
        artpres = (art_visc[1:] - art_visc[:-1]) / self.dz_c / rho_s[1:-1]
        self.vx[1:-1] += (self.psi * np.cos(phi) * artpres) * dt
        self.vz[1:-1] += artpres * dt

        #######################################################
        # substep 2: Viscous forces ###########################
        #######################################################
        fv_x, fv_y, fv_z = self.f_viscous_jit(self.vx, self.vy, self.vz, self.psi, self.dz_s_g, self.dz_c,
                                              self.Omega_0, self.q, self.nu, self.rho[1:-1], phi)
        self.vx[1:-1] += fv_x * dt
        self.vy[1:-1] += fv_y * dt
        self.vz[1:-1] += fv_z * dt

        ##########################################
        # Substep 3: Transport ###################
        ##########################################

        # momentum calculation
        advec_vel = self.vz + self.psi * np.cos(phi) * self.vx
        self.mom_xm[1:-1], self.mom_xp[1:-1] = self.mom_mp(self.rho, self.vx)
        self.mom_ym[1:-1], self.mom_yp[1:-1] = self.mom_mp(self.rho, self.vy)
        self.mom_zm[1:-1], self.mom_zp[1:-1] = self.mom_mp(self.rho, self.vz)

        # momentum update
        dt_dz = dt / self.dz
        self.mom_zm[2:-2] += dt_dz * self.vLs(self.mom_zm, advec_vel[1:-1], self.dz, dt)
        self.mom_zp[2:-2] += dt_dz * self.vLs(self.mom_zp, advec_vel[1:-1], self.dz, dt)
        self.mom_xm[2:-2] += dt_dz * self.vLs(self.mom_xm, advec_vel[1:-1], self.dz, dt)
        self.mom_xp[2:-2] += dt_dz * self.vLs(self.mom_xp, advec_vel[1:-1], self.dz, dt)
        self.mom_ym[2:-2] += dt_dz * self.vLs(self.mom_ym, advec_vel[1:-1], self.dz, dt)
        self.mom_yp[2:-2] += dt_dz * self.vLs(self.mom_yp, advec_vel[1:-1], self.dz, dt)

        # density update
        self.rho_g[1:-1] = self.rho
        self.rho[1:-1] += dt_dz * self.vLs(self.rho_g, advec_vel, self.dz, dt)

        # new velocity calculation and update
        self.vx[1:-1] = self.v_from_mom(self.mom_xm, self.mom_xp, self.rho)
        self.vy[1:-1] = self.v_from_mom(self.mom_ym, self.mom_yp, self.rho)
        self.vz[1:-1] = self.v_from_mom(self.mom_zm, self.mom_zp, self.rho)

        ##########################################
        # substep 4: damping boundary conditions #
        ##########################################
        if False:
            new_hp = 1 / (np.sqrt(2*np.pi) * np.max(self.rho))
            damp2rho = gaussian(self.z_c_g, new_hp, 1)
        if self.boundaries.damp_rho:
            self.rho[:] = self.damping_bc(self.rho, self.initial_conditions.rho, dt, self.inv_damp_c)
            # self.rho[:] = self.damping_bc(self.rho, damp2rho, dt, self.inv_damp_c)
        if self.boundaries.damp_vx:
            self.vx[:] = self.damping_bc(self.vx, self.initial_conditions.vx, dt, self.inv_damp_s)
        if self.boundaries.damp_vy:
            self.vy[:] = self.damping_bc(self.vy, self.initial_conditions.vy, dt, self.inv_damp_s)
        if self.boundaries.damp_vz:
            self.vz[:] = self.damping_bc(self.vz, self.initial_conditions.vz, dt, self.inv_damp_s)

        ########
        # dust #
        ########
        if self.use_dust:
            self.dust.X = self.dust.X + dt*self.dust_eom(self.dust.X + .5*dt*self.dust_eom(self.dust.X, phi), phi)

            # drag force
            dust_loc = self.dust.X[0]
            dust_v = self.dust.X[1:]
            ln_rho = np.log(self.rho)
            dz_ln_rho = (ln_rho[1:] - ln_rho[:-1]) / self.dz_c_g
            # gas_vz_at_dust_pos = TSC_jit(dust_loc, self.z_s_g, self.vz, self.dz_s_g[0])
            gas_vz_at_dust_pos, gas_dens_grad_at_dust_loc = TSC_fast(dust_loc, self.z_s_g, self.vz, dz_ln_rho, self.dz_s_g[0])
            self.dust.X[1:] = (dust_v + dt*gas_vz_at_dust_pos*self.dust.coupling) / (1 + dt*self.dust.coupling)

            if self.dust.diffusion:
                delta_s = np.sqrt(2 * self.nu / (1 + self.dust.St ** 2) * dt)
                gaussian_mean = self.nu/(1 + self.dust.St ** 2) * gas_dens_grad_at_dust_loc * dt
                self.dust.X[0] += np.random.normal(gaussian_mean, delta_s, len(self.dust.X[0]))

        self.t += dt
        self.N_steps_total += 1

    @staticmethod
    @conditional_decorator(jit(cache=use_cache, parallel=use_parallel), use_numba)
    def mom_mp(rho, v):
        """Calculates the left and right momenta (- and +)"""
        dummy = rho[1:-1] * v[:-1], rho[1:-1] * v[1:]
        return dummy

    @staticmethod
    @conditional_decorator(jit(cache=use_cache, parallel=use_parallel), use_numba)
    def v_from_mom(mom_m, mom_p, rho):
        """"Calculates velocity from left and right momentum, and density"""
        return (mom_m[2:-1] + mom_p[1:-2]) / (rho[2:-1] + rho[1:-2])

    @staticmethod
    @conditional_decorator(jit(cache=use_cache, parallel=use_parallel), use_numba)
    def vLs(Q, advec_vel, dz, dt):
        """van Leer slopes for any face-centered variable.
        Returns the total flux for cell i (left and right)"""
        DQ_p_half = (Q[2:] - Q[1:-1]) / dz
        DQ_m_half = (Q[1:-1] - Q[:-2]) / dz

        vLs = 2 * DQ_p_half * DQ_m_half / (DQ_p_half + DQ_m_half)  # * (DQ_p_half * DQ_m_half >= 0)
        vLs[DQ_p_half * DQ_m_half <= 0] = 0
        Q_i = Q[1:-2] + vLs[:-1] * (dz - advec_vel * dt) / 2
        Q_ip1 = Q[2:-1] - vLs[1:] * (dz + advec_vel * dt) / 2
        flux_full = advec_vel * (Q_i * (advec_vel > 0) + Q_ip1 * (advec_vel < 0))

        return flux_full[:-1] - flux_full[1:]

    def dust_eom(self, X, phi):
        """Equation of motion for a (dust) particle."""
        z, vx, vy, vz = X
        return np.array([vz + self.psi*vx*np.cos(phi),
                         2*self.Omega_0*vy,
                         (self.q-2)*self.Omega_0*vx,
                         -self.psi*self.Omega_0*np.sin(phi)*vx - self.Omega_0**2*z])

    @staticmethod
    @conditional_decorator(jit(cache=True, parallel=use_parallel), use_numba)
    def f_viscous_jit(vx, vy, vz, psi, dz_s_g, dz_c, Omega_0, q, nu, rho, phi):
        """Static version of the viscous force calculation
        (for numba support)."""
        dz_vx = (vx[1:] - vx[:-1]) / dz_s_g
        dz_vy = (vy[1:] - vy[:-1]) / dz_s_g
        dz_vz = (vz[1:] - vz[:-1]) / dz_s_g

        psicosphi = psi*np.cos(phi)

        s_xx = 4/3*psicosphi*dz_vx - 2/3*dz_vz
        s_zx = dz_vx + psicosphi*dz_vz + psi*Omega_0*np.sin(phi)  # equal to s_xz
        s_xy = psicosphi*dz_vy - q*Omega_0
        s_zy = dz_vy
        s_zz = 4/3*dz_vz - 2/3*psicosphi*dz_vx

        rho_nu = nu * rho
        t_xx = rho_nu * s_xx
        t_zx = rho_nu * s_zx
        t_xy = rho_nu * s_xy
        t_zy = rho_nu * s_zy
        t_zz = rho_nu * s_zz

        dz_t_xx = (t_xx[1:] - t_xx[:-1]) / dz_c
        dz_t_zx = (t_zx[1:] - t_zx[:-1]) / dz_c
        dz_t_xy = (t_xy[1:] - t_xy[:-1]) / dz_c
        dz_t_zy = (t_zy[1:] - t_zy[:-1]) / dz_c
        dz_t_zz = (t_zz[1:] - t_zz[:-1]) / dz_c

        rho_s = (rho[1:] + rho[:-1]) / 2
        f_x = (psicosphi*dz_t_xx + dz_t_zx) / rho_s
        f_y = (psicosphi*dz_t_xy + dz_t_zy) / rho_s
        f_z = (psicosphi*dz_t_zx + dz_t_zz) / rho_s

        return f_x, f_y, f_z

    @staticmethod
    @conditional_decorator(jit(cache=True), use_numba)
    def damping_bc(x, x0, dt, inv_damp):
        """Solves the PDE f'(t)=-f(t)/tau to second order (midpoint rule)."""
        # TODO: Do this with backward Euler
        return x - dt * (x - x0) * (inv_damp - .5 * dt * inv_damp**2)

    def __save_data(self):
        if self.save_single:
            np.savez(f"{self.output_dir}/out{self.N_out}", rho=self.rho, vx=self.vx, vy=self.vy, vz=self.vz, t=self.t)
            if self.use_dust:
                np.save(f"{self.output_dir}/dout{self.N_out}.npy", self.dust.X)
                raise NotImplementedError
            self.N_out += 1
        else:
            self.data.append(State(*[np.copy(x) for x in (self.rho, self.vx, self.vy, self.vz)], self.t))
            if self.use_dust:
                self.data_d.append(deepcopy(self.dust))

    def save_to_file(self, filename: str, override=False):
        if not filename.endswith(".pkl"):
            filename += ".pkl"

        import os
        import pickle
        import re

        if os.path.isfile(filename):
            if override:
                print(f"File {filename} already exists, will be overwritten!")
            else:
                new_index = 1
                while os.path.isfile(filename):
                    filename = re.sub(r"(_\d*)?\.pkl$", f"_{new_index}.pkl", filename)
                    new_index += 1
                print(f"File already exists, renaming to {filename}!")

        with open(filename, 'wb') as outp:
            pickle.dump(self.__dict__, outp, pickle.HIGHEST_PROTOCOL)

    def load_from_file(self, filename: str):
        if not filename.endswith(".pkl"):
            filename += ".pkl"

        import pickle

        with open(filename, 'rb') as outp:
            tmp_dict = pickle.load(outp)
        self.__dict__.update(tmp_dict)
        print("Successfully loaded!")

############################################################
# ab hier alles weg ########################################
############################################################
if __name__ == "__main__":
    myfactor = 2.0 /5
    N_cells = 300 #int(100 * myfactor)
    my_hp = .05
    my_Omega_0 = np.sqrt(GM / r_0**3)
    my_cs = my_Omega_0 * my_hp
    my_z_max = .25*myfactor
    # TODO: my_z_max langsam erhöhen und Ränder beobachten
    my_z = np.linspace(-my_z_max, my_z_max, N_cells)

    my_alpha_t = 1e-2

    is_curved = False
    if is_curved:
        myrho = curved_gaussian(my_z, my_hp, 1)
    else:
        myrho = gaussian(my_z, my_hp, 1)

    myIC = State(myrho,
                 np.zeros(N_cells-1),     # vx
                 np.zeros(N_cells-1),     # vy
                 np.zeros(N_cells-1),     # vz
                 0)
    # TODO: psi langsam erhöhen: Rand oder Physik?

    my_bounds = BoundaryConditions("exponential", "zero", "zero", "zero",
                                   False, True, True, True)

    my_psi = .1*0
    my_T = T_irr(my_z, 0.05, 1e-6)
    fewdust = False
    if fewdust:
        dustN = 6
        dustZ = np.ones(dustN)*0.01
        dustSt = np.geomspace(1e-2, 10, dustN)
        dustSt[0] = 1e-9
    else:
        dustN = 3000
        const_St = .1
        dust_hp = np.sqrt(my_alpha_t*my_cs**2/(1 + const_St ** 2)/const_St)
        dustZ = np.random.normal(0, dust_hp, dustN)
        print(dust_hp)
        # dustZ = np.random.normal(0, my_hp, dustN)
        dustSt = np.ones(dustN)*const_St

    dustV = np.zeros((3, dustN))
    mydust = DustClass(np.vstack((dustZ, dustV)), dustSt, diffusion=True)
    mySim = Sim(myIC, boundaries=my_bounds, psi=my_psi, z_ghost=my_z,
                # cs=T_irr_shadowing,
                cs=my_T**.5 if False else my_cs,
                alpha_t=my_alpha_t, logs=True, curvature=is_curved, damping_tau=1.,
                dust=mydust,
                q=1.4938 if False else 1.5, psi_var=None) #(12, 3))
    mySim.simulate(dt_output=0.2, N_dt=int(50 / 0.2), save_single=False)
    N_tot = len(mySim.data)

    dust_time = np.array([dummy.t for dummy in mySim.data])
    dust_start = 0 #np.argmin(np.abs(dust_time-100))
    if True:
        if fewdust:
            dust_pos = np.array([dummy.X[0] for dummy in mySim.data_d])
            plt.figure(dpi=200)
            plt.ylim(-.15, .15)
            for i in range(dustN):
                plt.plot(dust_time[dust_start:], dust_pos[dust_start:, i], color=plt.get_cmap("viridis")(i/(dustN-1)), label=round(dustSt[i], 3))
                #plt.scatter(mySim.data[i].t, mySim.data_d[i].X[0][0], color=plt.get_cmap("viridis")(i/len(mySim.data_d)), s=1)
            #plt.xscale("log")
            plt.axhline(0, ls='dotted', alpha=.4, c='black')
            plt.legend()
            plt.show()

        if not fewdust:
            plt.figure(dpi=200)
            #plt.ylim(-.01, .01)
            plt.ylim(-4*5*dust_hp, 4*5*dust_hp) #plt.ylim(-.03, .03)
            dust_dens = np.zeros((len(mySim.z_c), len(dust_time)))
            for i in range(len(mySim.data)):
                dust_dens[:, i] = TSC(mySim.z_c, mySim.data_d[i].X[0], np.ones(dustN), mySim.dz_c[0])
                # plt.plot(dust_time, dust_pos[:, i], color=plt.get_cmap("viridis")(i/(dustN-1)), label=round(dustSt[i], 3))
                #plt.scatter(mySim.data[i].t*np.ones_like(mySim.z_c), mySim.z_c, color=plt.get_cmap("magma")(dust_dens/dust_dens.max()), s=20)
            #plt.xscale("log")
            dust_dens = np.log(dust_dens+1)
            plt.pcolormesh(dust_time, mySim.z_c, dust_dens, shading="auto")
            # plt.scatter(dust_time[1:], np.array(mySim.last_dt), c='red', s=3)
            plt.colorbar()
            plt.xlabel("time")
            plt.ylabel("z")
            plt.axhline(0, ls='dotted', alpha=.4, c='black')
            plt.tight_layout()
            plt.show()
            print("...")
    # vx and vy over time
    plt.figure(dpi=200)
    plt.plot(mySim.z_c/0.05, np.log(dust_dens)[:, -1])
    plt.show()
    if False:
        all_Vs = np.zeros((len(mySim.data), 3))
        for i in range(len(mySim.data)):
            all_Vs[i] = get_V(mySim, my_hp, i)

        plt.figure(dpi=200)
        myt = np.array([x.t for x in mySim.data])
        final_vx, final_vy = vip0(my_alpha_t, my_psi)
        # plt.axhline(final_vx, c="C0")
        # plt.axhline(final_vy, c="C1")
        # plt.axhline(0, c='black')
        plt.plot(myt, all_Vs[:, 0], marker=".", markersize=1, label="vx", c="C0")
        plt.plot(myt, all_Vs[:, 1], marker=".", markersize=1, label="vy", c="C1")
        plt.plot(myt, all_Vs[:, 2], marker=".", markersize=1, label="vz", c="C2")
        plt.legend()
        plt.xlabel("time")
        plt.ylabel("velocity")
        plt.show()

    # all velocities
    if False:
        plt.figure(dpi=200)
        for i in range(N_tot):
            if i % 10 == 0:
                # plt.plot(mySim.z_s_g, mySim.data[i].vx, label="vx", c="red")
                # plt.plot(mySim.z_s_g, mySim.data[i].vy, label="vy", c="blue")
                plt.plot(mySim.z_s_g, mySim.data[i].vz, label=i, c=plt.get_cmap("viridis")(i/N_tot))
                # plt.plot(mySim.z_c, (mySim.data[i].vz[1:]+mySim.data[i].vz[:-1])*mySim.data[i].rho/2,
                # label=i, c=plt.get_cmap("viridis")(i / N_tot))

        plt.ylabel(r"velocity")
        plt.xlabel("z_c")
        plt.title(f"N={N_cells}")
        plt.xlim(-.2, .2)
        plt.ylim(-.1, .1)
        plt.show()

    # density
    if False:
        plt.figure(dpi=200)
        for i in range(N_tot):
            if i % 10 == 0:
                plt.plot(mySim.z_c_g, mySim.data[i].rho,
                         c=plt.get_cmap("viridis")(i/N_tot),
                         lw=1, label=i)

        plt.legend()
        # plt.yscale("log")
        plt.ylabel(r"$\rho$")
        plt.xlabel("z_c")
        plt.title(f"N={N_cells}")
        plt.show()


def get_empty_state():
    """Helper function to quickly get legal parameters for Sim-initialization"""
    ec = np.zeros(11)
    es = np.zeros(10)
    return State(ec, es, es, es, 0), BoundaryConditions("zero", "zero", "zero", "zero", False, False, False, False), 0., ec, 0., 0.
