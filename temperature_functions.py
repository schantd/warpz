import numpy as np
from useful_funtions import gaussian

def T_irr(z, h_p_d, Sigma0_d, cs_mid=0.05):
    gauss = gaussian(z, h_p_d, Sigma0_d)
    N = len(z)
    dz = z[-1] - z[-2]
    intgauss = np.cumsum(gauss[int(N / 2):][::-1])[::-1] * dz
    tau = 1.7e9 * intgauss

    T4_eff = cs_mid ** 8

    T4_surf = np.exp(-tau) * 2 * T4_eff

    T_final = (T4_eff + T4_surf) ** .25

    return np.hstack((T_final[::-1], T_final))


def T_irr_shadowing(phi, z=np.linspace(-.25, .25, 100), h_p_d=0.025, Sigma0_d=1e-6, cs_mid=0.05):
    gauss = gaussian(z, h_p_d, Sigma0_d)
    N = len(z)
    dz = z[-1] - z[-2]
    intgauss = np.cumsum(gauss[int(N / 2):][::-1])[::-1] * dz
    tau = 1.7e9 * intgauss

    T4_eff = cs_mid ** 8

    T4_surf_up = np.exp(-tau) * 1.5 * T4_eff * np.minimum(1, (np.cos(phi)+1))**6
    T4_surf_down = np.exp(-tau) * 1.5 * T4_eff * np.minimum(1, (-np.cos(phi)+1))**6

    T_final_up = (T4_eff + T4_surf_up)**0.25
    T_final_down = (T4_eff + T4_surf_down)**0.25

    return np.hstack((T_final_down[::-1], T_final_up))**.5