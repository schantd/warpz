import astropy.units as u
import astropy.constants as const


# unit system is given by
# sign   units                   name
# G      [M_star^-1 L^3 T^-2]    gravitational constant
# M_star [M_star^1]              central star mass
# r_0    [L^1]                   a length
# k/mu   [L^2 T^-2 theta^-1]     ratio of Boltzmann-const. to mean molecular weight

G = 1.
M_star = 1.
GM = G * M_star
r_0 = 1.
komu = 1.  # "k over mu"

use_numba = False
use_parallel = False
use_cache = True


def convert(value, units: str):
    const_M = const.M_sun
    const_L = const.au
    const_G = const.G
    const_mean_molecular_weight = 2.3 * const.m_p
    const_k_over_mu = const.k_B / const_mean_molecular_weight

    cu_M = u.def_unit("cu_M", represents=const_M, doc="code unit mass")
    cu_L = u.def_unit("cu_L", represents=const_L, doc="code unit length")
    cu_T = u.def_unit("cu_T", represents=(const_L ** 3 / (const_G * const_M)) ** 0.5, doc="code unit time")
    cu_temp = u.def_unit("cu_temp", represents=(const_G * const_M / const_k_over_mu / const_L).to("K"),
                         doc="code unit temperature")
    cu_Power = u.def_unit("cu_Power", represents=const_M * const_L ** 2 * (1 * cu_T) ** -3, doc="code unit power")
    cu_Power2 = u.def_unit("cu_Power", represents=(const_G**3 * const_M**5 * const_L**-5)**0.5, doc="code unit power 2")

    u.add_enabled_units([cu_M, cu_L, cu_T, cu_temp, cu_Power, cu_Power2])

    return value.to(units).value
