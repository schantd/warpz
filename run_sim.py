if __name__ == "__main__":
    import numpy as np
    from units import GM, r_0
    from useful_funtions import gaussian, curved_gaussian
    from warped import Sim, BoundaryConditions, State, DustClass
    from math import ceil

    # Here we are going to define all our parameters
    my_psi = .01         # warp strength
    my_alpha_t = 1e-2    # turbulent viscosity parameter

    myres = 150
    # Boundary conditions (first four are type of BC, the bool whether to damp boundaries)
    my_bounds = BoundaryConditions("exponential", "zero", "zero", "zero",
                                   False, True, True, True)
    my_dt_output = np.pi/6         # time after which to save snapshots
    T_sim = 1 / my_alpha_t    # total simulation time
    my_N_dt = ceil(T_sim / my_dt_output)  # corresponding total number of snapshots to save

    # myfactor = 12.
    N_cells = 300
    my_hp = .05                     # pressure scale height
    my_z_max = 10 * my_hp
    my_Omega_0 = np.sqrt(GM / r_0**3)
    my_cs = my_Omega_0 * my_hp      # sound speed
    my_z = np.linspace(-my_z_max, my_z_max, N_cells)  # centered z-grid including ghost cells

    # if is_curved=False, the gravitational force is given by -z*Omega_0^2 (flat disk approximation)
    # if it is True, the force is given by - GM / (r^2 + z^2)^(3/2) * z
    # This also influences the equilibrium density distribution
    is_curved = False
    if is_curved:
        myrho = curved_gaussian(my_z, my_hp, 1)
    else:
        myrho = gaussian(my_z, my_hp, 1)

    # initial conditions
    myIC = State(myrho,                   # density
                 np.zeros(N_cells-1),     # vx
                 np.zeros(N_cells-1),     # vy
                 np.zeros(N_cells-1),     # vz
                 0)                       # t0

    # Initialize the simulation parameters
    mySim = Sim(
        myIC,                   # initial conditions
        boundaries=my_bounds,   # boundary conditions
        psi=my_psi,             # warp strength psi
        z_ghost=my_z,           # z-mesh (including ghost cells)
        cs=my_cs,               # value of the sound speed
        alpha_t=my_alpha_t,     # turbulent viscosity parameter
        logs=True,              # whether to print information while simulating
        curvature=is_curved,    # the kind of gravitational force to use
        damping_tau=.1,         # damping timescale
        )
    # start the simulation
    # save_single=True saves all snapshots individually as npz-files
    # save_single=False saves the whole simulation as one single pickle file (which can get enormous)
    mySim.simulate(dt_output=my_dt_output, N_dt=my_N_dt, output_dir=f"example1", save_single=True)

    del mySim

else:
    print("Please run this as a main script.")
