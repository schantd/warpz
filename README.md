# Welcome to warpz
#### A completely undocumented and sparsely commented simulation code 

Warpz solves the laminar warped shearing box equations in the vertical direction.

To get started, just take a look at run_sim.py, which you can also run to run an example simulation  (but create a directory named "outputs" first).
Then, you can run plot_data.py to plot some results.
For further questions, just contact me.

