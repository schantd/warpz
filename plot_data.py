import numpy as np
import matplotlib.pyplot as plt
import pickle

# from useful_funtions import get_V, vip0, TSC_fast, TSC_jit
# from warped import Sim, BoundaryConditions, State, get_empty_state

parameter_filename = "outputs/example1/params.pkl"
with open(parameter_filename, 'rb') as temp:
    parameters = pickle.load(temp)
z_c = parameters["z_c_g"]  # centered coordinates
z_s = parameters["z_s_g"]  # staggered coordinates
h_p = 0.05
data = np.load("outputs/example1/out100.npz")

vx = data["vx"]
vy = data["vy"]
vz = data["vz"]
rho = data["rho"]
time = data["t"].item()


fig, axs = plt.subplots(2, 2, dpi=150, )
fig.suptitle(f"snapshots at $t$={time}")

axs[0,0].plot(z_c/h_p, rho)
axs[1,0].plot(z_s/h_p, vx)
axs[0,1].plot(z_s/h_p, vy)
axs[1,1].plot(z_s/h_p, vz)

axs[0,0].set_title(r"$\rho$")
axs[1,0].set_title(r"$v_x'$")
axs[0,1].set_title(r"$v_y'$")
axs[1,1].set_title(r"$v_z'$")

for ax in axs.flatten():
    ax.set_xlabel(r"$z'/h_p$")


plt.show()